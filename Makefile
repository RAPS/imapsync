
# $Id: Makefile,v 1.301 2019/05/28 14:25:35 gilles Exp gilles $	

.PHONY: help usage 

help: usage

usage:
	@echo "this is imapsync 1.945, You can do :"
	@echo "make install # as root"
	@echo ""

PREFIX ?= /usr

clean: clean_man

.PHONY: install man

man:  W/imapsync.1

clean_man:
	rm -f  W/imapsync.1

W/imapsync.1: imapsync
	pod2man imapsync > W/imapsync.1

install: W/imapsync.1
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install imapsync $(DESTDIR)$(PREFIX)/bin/imapsync
	chmod 755 $(DESTDIR)$(PREFIX)/bin/imapsync
	mkdir -p $(DESTDIR)$(PREFIX)/share/man/man1
	install W/imapsync.1 $(DESTDIR)$(PREFIX)/share/man/man1/imapsync.1
	chmod 644 $(DESTDIR)$(PREFIX)/share/man/man1/imapsync.1


